#include"header.h"
#include"structures.h"
#include"declarations.h"

void* processing(void *arg)
{
	int sockfd, fd, ret;
	char oper;
	fd = *(int *)arg;  				
	static int clients = 1;
	request client_data;
	receive result;
	printf("fd = %d\n", fd);
	read(fd, &client_data, sizeof(client_data));
	
	ret = fork();   // child process for EXECLing into other processing clients
        switch(ret)
        {
                case -1:
                        perror("fork()");
                        exit(EXIT_FAILURE);
                case 0:                                 // child
                        switch(oper)
                        {
                                case '+': execl("./adder","adder", NULL);
                                case '-': execl("./subtractor","subtractor", NULL);
                                case '*': execl("./multiplier","multipier", NULL);
                                case '/': execl("./divider","divider", NULL);
                                default:
                                        printf("Invalid operator choice....going on block !!\n");
                        }
                        break;
                default:                                // parent
                        printf("This is req_processing unit\n");
	
	write(fd, &result, sizeof(result));
	close(fd);
	printf("Server wrote : %c to client number : %d\n", ch, clients);
	printf("##############################################\n\v");
	clients++;
}
