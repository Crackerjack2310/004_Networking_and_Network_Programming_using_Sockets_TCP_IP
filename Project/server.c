#include"header.h"
#include"structures.h"
#include"declarations.h"
#define BACKLOG 100

int main()
{
	int sockfd, on = 1, pret, i = 0, fd[100000];
        struct sockaddr_in server_addr;	
	socklen_t addr_size;	
	pthread_t tid[100000];
	addr_size = sizeof(server_addr);

        sockfd = socket(AF_INET, SOCK_STREAM, 0);	// returns a fd for our socket
        if (sockfd == -1)	
	{
		perror("socket");
		exit(1);
	}
	printf("socket call returned : %d\n", sockfd);
	memset(&server_addr, 0, sizeof(server_addr));                      /* Clear structure */
        server_addr.sin_family = AF_INET;
        server_addr.sin_port = htons(7777);			// host to network (short)
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);	// allow all incoming connections to servers
	/*Any connections with the same target IP will be accepted by server*/
	setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
	/*enabling the REUSE of same PORT no.*/

	/*Now we give a name to socket*/
	if (bind(sockfd, (struct sockaddr*) &server_addr, sizeof(server_addr)) == -1)
	{
		perror("bind");
		exit(1);	
	}
	
	/*create listen queue, size defined by BACKLOG value, clients wait in here, till server busy*/
	if (listen(sockfd, BACKLOG) == -1)	
	{
		perror("listen");
		exit(1);
	}
	
	while (1)
	{
        	puts("Server waiting for client ...\n");
		fd[i] = accept(sockfd, (struct sockaddr*) &server_addr, &addr_size);	
		/* stays on block till no client approaches, else create a file descriptor */
		/* we need array here as a single int value wdould be updated, causing further issues*/
		/* now we need a thread to take care of this client, while the server goes on accept*/
		pret = pthread_create(&tid[i], NULL, &processing, (void *) &fd[i]);
		if (pret == -1)
		{
			perror("pthread_create()");
			exit(1);
		}
		i++;
	}
}
