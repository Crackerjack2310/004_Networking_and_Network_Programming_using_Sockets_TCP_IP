#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<fcntl.h>
#include<time.h>
#include<math.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<signal.h>
#include<sys/ipc.h>
#include<semaphore.h>
#include<sys/shm.h>
#include<sys/sem.h>
#include<sys/msg.h>

#include<pthread.h>

#include<sys/socket.h>
#include<sys/un.h>
#include<netinet/in.h>
#include<arpa/inet.h>
