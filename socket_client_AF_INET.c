#include"header.h"
#include"structures.h"
#include"declarations.h"

int main()
{
	int sockfd, ret;
        struct sockaddr_in peer_addr;
	socklen_t addr_size;

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd == -1)	
	{
		perror("socket");
		exit(1);
	}
	printf("socket returned : %d\n", sockfd);
	memset(&peer_addr, 0, sizeof(struct sockaddr_in));	      /* Clear structure */
	peer_addr.sin_family = AF_INET;
        peer_addr.sin_port = htons(7777);
        peer_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 		// loopback address for within the system
	addr_size = sizeof(peer_addr);
        ret = connect(sockfd, (struct sockaddr*) &peer_addr, addr_size);
	if (ret == -1)
	{ 
		perror("connect() failed : ");
		fprintf(stderr,"error no : %d\n", errno);
		exit(1);
	}	
	char ch = 'A';
	sleep(1);
	write(sockfd, &ch, 1);
	read(sockfd, &ch, 1);
	printf("Got result from server : %c\n", ch);
	close(sockfd);
}
