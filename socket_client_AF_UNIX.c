#include"header.h"
#include"structures.h"
#include"declarations.h"

int main()
{
	int sockfd, ret;
        struct sockaddr_un peer_addr;
	socklen_t addr_size;

        sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
        if (sockfd == -1)	
	{
		perror("socket");
		exit(1);
	}
	memset(&peer_addr, 0, sizeof(struct sockaddr_un));
                              /* Clear structure */
        peer_addr.sun_family = AF_UNIX;
        strncpy(peer_addr.sun_path, "socket", sizeof(peer_addr.sun_path) - 1);

	addr_size = sizeof(peer_addr);
        ret = connect(sockfd, (struct sockaddr*) &peer_addr, addr_size);
        if (ret == -1)
	{ 
		perror("connect");
		exit(1);
	}	
	char ch = 'A';
	sleep(1);
	write(sockfd, &ch, 1);
}
