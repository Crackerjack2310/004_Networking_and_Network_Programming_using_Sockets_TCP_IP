#include"header.h"
#include"structures.h"
#include"declarations.h"
#define BACKLOG 1

int main()
{
	int sockfd, fd, on = 1;
        struct sockaddr_in server_addr;	
	socklen_t addr_size;	
					
        sockfd = socket(AF_INET, SOCK_STREAM, 0);	
        if (sockfd == -1)	
	{
		perror("socket");
		exit(1);
	}
	printf("socket call returned : %d\n", sockfd);
	memset(&server_addr, 0, sizeof(server_addr));                      /* Clear structure */
        server_addr.sin_family = AF_INET;
        server_addr.sin_port = htons(7777);			// host to network (short)
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);	// allow all incoming connections to servers

	setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof (on));

        if (bind(sockfd, (struct sockaddr*) &server_addr, sizeof(server_addr)) == -1)
	{
		perror("bind");
		exit(1);	
	}
	
	if (listen(sockfd, BACKLOG) == -1)	
	{
		perror("listen");
		exit(1);
	}
	addr_size = sizeof(server_addr);
	while (1)
	{
        	puts("Server waiting\n");
		fd = accept(sockfd, (struct sockaddr*) &server_addr, &addr_size);	
		/* stays on block till no client approaches else creates a file descriptor */
       		if (fd == -1)
		{ 
			perror("accept");
			exit(1);
		}	
		printf("accept returned : %d\n", fd);
		char ch;
		read(fd, &ch, 1);
		printf("server read : %c\n",ch);	
		ch++;
		write(fd, &ch, 1);
		close(fd);
	}
}
