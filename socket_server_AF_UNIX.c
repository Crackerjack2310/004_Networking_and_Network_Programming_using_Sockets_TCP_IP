#include"header.h"
#include"structures.h"
#include"declarations.h"

int main()
{
	system("unlink socket");
	int sockfd, cfd;
        struct sockaddr_un server_addr;
	socklen_t addr_size;

        sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
        if (sockfd == -1)	
	{
		perror("socket");
		exit(1);
	}
	
	memset(&server_addr, 0, sizeof(struct sockaddr_un));                      /* Clear structure */
        server_addr.sun_family = AF_UNIX;
        strncpy(server_addr.sun_path, "socket", sizeof(server_addr.sun_path) - 1);
										/*Giving a name to our socket*/
       	if (bind(sockfd, (struct sockaddr*) &server_addr, sizeof(struct sockaddr_un)) == -1)
	{
		perror("bind");
		exit(1);	
	}

	if (listen(sockfd, 1) == -1)
	{
		perror("listen");
		exit(1);
	}
	addr_size = sizeof(server_addr);
        cfd = accept(sockfd, (struct sockaddr*) &server_addr, &addr_size);
        if (cfd == -1)
	{ 
		perror("accept");
		exit(1);
	}	
	char ch;
	read(cfd, &ch, 1);
	printf("server read : %c\n",++ch);	
}
